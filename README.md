## Sync SDK
The Sync SDK will allow you to create a solution for synchronizing content.

**( * ) SDKs are accessible only from the private area**

Need technical support? contact us at [Support@sceenic.co](mailto:Support@sceenic.co)

## Example description
This project is an example of SyncSDK integration using React (Typescript).
It contains full functionality and a list of example players.

## Tutorial
1. Retrieve `NPM_TOKEN` from [private area](https://media.sceenic.co/)
2. Copy `.npmrc.example` to `.npmrc` and replace `YOUR_NPM_TOKEN` with `NPM_TOKEN` obtained in a previous step
3. Run `npm i`
4. Run `npm start`

_Note_: some players may require license key set in `.env`

## Documents
Have a look at our official documentation [Sceenic](https://documentation.sceenic.co)
