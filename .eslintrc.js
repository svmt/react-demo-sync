module.exports = {
  extends: [
    "react-app",
  ],
  "overrides": [
    {
      "files": ["**/*.ts?(x)"],
      "rules": {
        "import/no-anonymous-default-export": "off"
      }
    }
  ]
};
