import { isSafariOriOS } from '../helper/device';

export default [
    { name: 'Football (PROGRAM-DATE-TIME)', video: 'https://demo-app.sceenic.co/football.m3u8' },
    { name: 'Moctobltc', video: 'https://moctobpltc-i.akamaihd.net/hls/live/571329/eight/playlist.m3u8' },
    { name: 'Tears of steel', video: 'https://cph-p2p-msl.akamaized.net/hls/live/2000341/test/master.m3u8' },
    ...(!isSafariOriOS()
        ? [
              {
                  name: 'Bip bop 4X3',
                  video: 'https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8',
              },
          ]
        : []),
    {
        name: 'Bip bop 16X9',
        video: 'https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_16x9/bipbop_16x9_variant.m3u8',
    },
    {
        name: 'ID3 tags',
        video: 'https://eu-dev.stream.easelive.tv/fotball/ngrp:Stream1_all/playlist.m3u8',
    },
    {
        name: 'Long VOD',
        video: 'https://vod-ffwddevamsmediaservice.streaming.mediaservices.windows.net/6e8b64ef-feb3-4ea9-9c63-32ef7b45e1dd/6e8b64ef-feb3-4ea9-9c63-32ef7b45.ism/manifest(format=m3u8-aapl,filter=hls)',
    },
];
