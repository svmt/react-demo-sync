export default [
    { name: 'Football (PROGRAM-DATE-TIME)', video: 'https://demo-app.sceenic.co/football.m3u8' },
    {
        name: 'Bip bop 4X3',
        video: 'https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8',
    },
    {
        name: 'Bip bop 16X9',
        video: 'https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_16x9/bipbop_16x9_variant.m3u8',
    },
    { name: 'Rainbow Timer', video: 'https://livesim.dashif.org/livesim/testpic_2s/Manifest.mpd' },
    {
        name: 'Low latency',
        video: 'https://livesim.dashif.org/livesim/chunkdur_1/ato_7/testpic4_8s/Manifest300.mpd',
    },
];
