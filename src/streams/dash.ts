export default [
    { name: 'Rainbow Timer', video: 'https://livesim.dashif.org/livesim/testpic_2s/Manifest.mpd' },
    {
        name: 'Low latency',
        video: 'https://livesim.dashif.org/livesim/chunkdur_1/ato_7/testpic4_8s/Manifest300.mpd',
    },
];
