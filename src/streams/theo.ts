export default [
    { name: "Football (PROGRAM-DATE-TIME)", video: 'https://demo-app.sceenic.co/football.m3u8' },
    {
        name: 'VOD',
        video:
            'https://multiplatform-f.akamaihd.net/i/multi/april11/sintel/sintel-hd_,512x288_450_b,640x360_700_b,768x432_1000_b,1024x576_1400_m,.mp4.csmil/master.m3u8',
    },
    {
        name: 'Bip bop 4X3',
        video: 'https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8',
    },
    {
        name: 'Bip bop 16X9',
        video: 'https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_16x9/bipbop_16x9_variant.m3u8',
    },
];
