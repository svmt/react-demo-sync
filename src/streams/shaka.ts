import { isSafariOriOS } from '../helper/device';

function streams() {
    if (isSafariOriOS()) {
        return [
            { name: 'Football (PROGRAM-DATE-TIME)', video: 'https://demo-app.sceenic.co/football.m3u8' },
            {
                name: 'Bip bop 4X3',
                video: 'https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8',
            },
            {
                name: 'Bip bop 16X9',
                video: 'https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_16x9/bipbop_16x9_variant.m3u8',
            },
            {
                name: 'Long VOD',
                video: 'https://vod-ffwddevamsmediaservice.streaming.mediaservices.windows.net/6e8b64ef-feb3-4ea9-9c63-32ef7b45e1dd/6e8b64ef-feb3-4ea9-9c63-32ef7b45.ism/manifest(format=m3u8-aapl,filter=hls)',
            },
            { name: 'Rainbow Timer', video: 'https://livesim.dashif.org/livesim/testpic_2s/Manifest.mpd' },
        ];
    }

    return [
        {
            name: 'Long VOD',
            video: 'https://vod-ffwddevamsmediaservice.streaming.mediaservices.windows.net/6e8b64ef-feb3-4ea9-9c63-32ef7b45e1dd/6e8b64ef-feb3-4ea9-9c63-32ef7b45.ism/manifest(format=m3u8-aapl,filter=hls)',
        },
        { name: 'Rainbow Timer', video: 'https://livesim.dashif.org/livesim/testpic_2s/Manifest.mpd' },
        {
            name: 'Low latency',
            video: 'https://livesim.dashif.org/livesim/chunkdur_1/ato_7/testpic4_8s/Manifest300.mpd',
        },
    ];
}

export default streams();
