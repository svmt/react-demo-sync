import React, { useEffect } from 'react';
import { Form, Formik } from 'formik';
import { FormControl, InputLabel, MenuItem, Select } from '@material-ui/core';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';
import { stopSynchronize } from '../../services/SynchronizeService';
import styles from './DemoList.css';
import { isSafariOriOS } from '../../helper/device';

interface SelectDemo {
    selectDemo: string;
}

const groupValidation = Yup.object({
    selectDemo: Yup.string(),
});

const DemoList = () => {
    const classes = styles();
    const history = useHistory();

    const initialValues: SelectDemo = { selectDemo: 'html' };

    const demoSelected = (values: any) => {
        history.push({ pathname: '/create-room', search: `?demo=${values.selectDemo}` });
    };

    useEffect(() => {
        stopSynchronize();
    }, []);

    return (
        <div className={classes.groupSelectionContainer}>
            <div className={classes.formContainer}>
                <div>
                    <Formik initialValues={initialValues} validationSchema={groupValidation} onSubmit={demoSelected}>
                        {({ handleChange, values, errors }) => (
                            <Form className={classes.groupForm}>
                                <FormControl className={classes.groupSelect}>
                                    <InputLabel
                                        classes={{
                                            root: classes.whitelabel,
                                            focused: classes.whitelabel,
                                            shrink: classes.whitelabel,
                                        }}
                                        htmlFor="watch-select"
                                    >
                                        Join demo
                                    </InputLabel>
                                    <Select
                                        labelId="watch-select"
                                        id="selectDemo"
                                        name="selectDemo"
                                        value={values.selectDemo}
                                        onChange={handleChange}
                                        className={classes.select}
                                        inputProps={{
                                            classes: {
                                                icon: classes.icon,
                                                root: classes.whitelabel,
                                            },
                                        }}
                                    >
                                        <MenuItem value="html">Html Player</MenuItem>
                                        <MenuItem value="youtube">YouTube Player</MenuItem>
                                        {/*<MenuItem value="nexplayer">Nex Player</MenuItem>*/}
                                        <MenuItem value="shakaplayer">Shaka Player</MenuItem>
                                        <MenuItem value="flowplayer">Flow Player</MenuItem>
                                        {/*<MenuItem value="voplayer">VO Player</MenuItem>*/}
                                        <MenuItem value="bitmovin">Bitmovin Player</MenuItem>
                                        {!isSafariOriOS() && <MenuItem value="brightcove">Brightcove Player</MenuItem>}
                                        <MenuItem value="vimeo">Vimeo Player</MenuItem>
                                        {/*<MenuItem value="theo">Theo Player</MenuItem>*/}
                                    </Select>
                                </FormControl>
                                <button className="watch-together-button">Select</button>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        </div>
    );
};
export default DemoList;
