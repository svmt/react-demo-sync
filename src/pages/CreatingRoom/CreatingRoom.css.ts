import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
    groupSelectionContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1,
    },
    formContainer: {
        width: 400,
        height: 300,
        border: '1px solid #fff',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    formControl: {
        minWidth: 120,
        marginTop: 10,
        marginBottom: 10,
    },
    groupNameLabel: {
        marginBottom: 10,
        color: '#fff',
    },
    groupForm: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
        fontFamily: 'myriad-pro',
    },
    groupSelect: {
        marginBottom: 20,
    },
    select: {
        '&:before': {
            borderColor: '#fff',
        },
        '&:after': {
            borderColor: '#fff',
        },
        '&:selected': {
            borderColor: '#fff',
        },
        '&:hover': {
            borderColor: '#fff!important',
        },
        '&:hover:not(.Mui-disabled):before': {
            borderColor: '#fff',
        },
    },
    icon: {
        fill: '#fff',
    },
    whitelabel: {
        color: '#fff!important',
    },
    label: {},
    errorLabel: {
        color: '#ef5959',
    },
});
