import React, { useEffect, useState } from 'react';
import { Form, Formik } from 'formik';
import { FormControl, InputLabel, MenuItem, Select } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { createGroup, initializeSyncSdk, stopSynchronize } from '../../services/SynchronizeService';
import { on, off } from '../../services/PubSub';
import { setRoom } from '../../store/main/actionCreator';
import hlsStreams from '../../streams/hls';
import flowStreams from '../../streams/flow';
import bitmovinStreams from '../../streams/bitmovin';
import htmlStreams from '../../streams/html';
import youtubeStreams from '../../streams/youtube';
import vimeoStreams from '../../streams/vimeo';
import theoStreams from '../../streams/theo';
import voStreams from '../../streams/vo';
import brightcoveStreams from '../../streams/brightcove';
import shakaStreams from '../../streams/shaka';
import styles from './CreatingRoom.css';

interface MyFormValues {
    groupName: string;
    clientName: string;
    token: string;
    selectedStream: string;
}

const schema = {
    clientName: Yup.string().required('Please enter your name'),
    selectedStream: Yup.string().required(),
    groupName: Yup.string(),
    token: Yup.string(),
};

if (process.env.REACT_APP_TOKEN) {
    schema.groupName = Yup.string().required('Please enter group name');
} else {
    schema.token = Yup.string().required('Please enter token');
}

const groupValidation = Yup.object(schema);

const CreatingRoom: React.FC<any> = () => {
    const classes = styles();
    const history = useHistory();
    const dispatch = useDispatch();
    const [demo, setDemo] = useState('');
    const [streamList, setStreamList] = useState<any>(hlsStreams);

    function handleError(error: any) {
        toast(error?.message, { type: 'error' });
    }

    useEffect(() => {
        stopSynchronize();
    }, []);

    useEffect(() => {
        const params = new URLSearchParams(history.location.search);
        const demo = params.get('demo');

        // @ts-ignore
        setDemo(demo);

        const streams = {
            shakaplayer: shakaStreams,
            flowplayer: flowStreams,
            html: htmlStreams,
            bitmovin: bitmovinStreams,
            vimeo: vimeoStreams,
            youtube: youtubeStreams,
            theo: theoStreams,
            voplayer: voStreams,
            brightcove: brightcoveStreams,
        };

        // @ts-ignore
        setStreamList(streams[demo] ?? hlsStreams);

        initializeSyncSdk();

        on('error', handleError);

        return () => {
            off('error', handleError);
        };
    }, [history.location.search]);

    const initialValues: MyFormValues = {
        groupName: '',
        clientName: '',
        selectedStream: streamList[0].name,
        token: '',
    };

    const handleSubmit = async (values: any, actions: any) => {
        actions.setSubmitting(false);
        const { groupName, selectedStream, clientName, token } = values;

        const stream = streamList.filter((item: any) => item.name === selectedStream)[0];
        const room = {
            groupName: values.groupName,
            token: values.token,
            clientName: values.clientName,
            video: stream.video,
        };

        // @ts-ignore
        dispatch(setRoom(room));
        await createGroup({ token, groupName, clientName });

        // @ts-ignore
        history.push(`/room/${demo}`);
    };

    return (
        <div className={classes.groupSelectionContainer}>
            <div className={classes.formContainer}>
                <div>
                    <Formik
                        initialValues={initialValues}
                        validationSchema={groupValidation}
                        onSubmit={handleSubmit}
                        enableReinitialize
                    >
                        {({ handleChange, values, errors }) => (
                            <Form className={classes.groupForm}>
                                {!process.env.REACT_APP_TOKEN ? (
                                    <>
                                        <label
                                            className={errors.token ? classes.errorLabel : classes.groupNameLabel}
                                            htmlFor="token"
                                        >
                                            {errors.token || 'Enter token'}
                                        </label>
                                        <input
                                            id="token"
                                            name="token"
                                            className="groupName-input"
                                            value={values.token}
                                            placeholder={'Enter token'}
                                            onChange={handleChange}
                                        />
                                    </>
                                ) : (
                                    <>
                                        <label
                                            className={errors.groupName ? classes.errorLabel : classes.groupNameLabel}
                                            htmlFor="groupName"
                                        >
                                            {errors.groupName || 'Enter group name'}
                                        </label>
                                        <input
                                            id="groupName"
                                            name="groupName"
                                            className="groupName-input"
                                            value={values.groupName}
                                            placeholder={'Enter group name'}
                                            onChange={handleChange}
                                        />
                                    </>
                                )}
                                <label
                                    className={errors.clientName ? classes.errorLabel : classes.groupNameLabel}
                                    htmlFor="clientName"
                                >
                                    {errors.clientName || 'Enter client name'}
                                </label>
                                <input
                                    id="clientName"
                                    name="clientName"
                                    className="groupName-input"
                                    value={values.clientName}
                                    placeholder={'Enter client name'}
                                    onChange={handleChange}
                                />

                                <FormControl className={classes.groupSelect}>
                                    <InputLabel
                                        classes={{
                                            root: classes.whitelabel,
                                            focused: classes.whitelabel,
                                            shrink: classes.whitelabel,
                                        }}
                                        htmlFor="watch-select"
                                    >
                                        Join stream
                                    </InputLabel>
                                    <Select
                                        labelId="watch-select"
                                        id="selectedStream"
                                        name={'selectedStream'}
                                        value={values.selectedStream}
                                        onChange={handleChange}
                                        className={classes.select}
                                        inputProps={{
                                            classes: {
                                                icon: classes.icon,
                                                root: classes.whitelabel,
                                            },
                                        }}
                                    >
                                        {streamList.map((item: any) => {
                                            return (
                                                <MenuItem key={item.name} value={item.name}>
                                                    {item.name}
                                                </MenuItem>
                                            );
                                        })}
                                    </Select>
                                </FormControl>

                                <button
                                    className="watch-together-button"
                                    disabled={!values.groupName && !values.selectedStream}
                                >
                                    Watch
                                </button>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        </div>
    );
};

export default CreatingRoom;
