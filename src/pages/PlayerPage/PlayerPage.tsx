import React from 'react';
import { useParams } from 'react-router-dom';
import SyncStats from '../../components/SyncStats/SyncStats';
import SyncGroupControls from '../../components/SyncGroupControls/SyncGroupControls';
import SyncControls from '../../components/SyncControls/SyncControls';
import ClientList from '../../components/ClientList/ClientList';
import SyncIndicator from '../../components/SyncIndicator/SyncIndicator';
import HtmlPlayer from '../../components/Players/HtmlPlayer';
import YoutubePlayer from '../../components/Players/YoutubePlayer';
import BitmovinPlayer from '../../components/Players/BitmovinPlayer';
import VimeoPlayer from '../../components/Players/VimeoPlayer';
import TheoPlayer from '../../components/Players/TheoPlayer';
import ShakaPlayer from '../../components/Players/ShakaPlayer';
import FlowPlayer from '../../components/Players/FlowPlayer';
import NexPlayer from '../../components/Players/NexPlayer';
import VOPlayer from '../../components/Players/VOPlayer';
import BrightcovePlayer from '../../components/Players/BrightcovePlayer';
import styles from './PlayerPage.css';

const PLAYERS = {
    'html': HtmlPlayer,
    'youtube': YoutubePlayer,
    'bitmovin': BitmovinPlayer,
    'vimeo': VimeoPlayer,
    'theo': TheoPlayer,
    'shakaplayer': ShakaPlayer,
    'flowplayer': FlowPlayer,
    'nexplayer': NexPlayer,
    'voplayer': VOPlayer,
    'brightcove': BrightcovePlayer,
};

const PlayerPage: React.FC = () => {
    const classes = styles();
    // @ts-ignore
    const { player } = useParams();

    // @ts-ignore
    const Player = PLAYERS[player];

    return (
        <div className={classes.pageContainer}>
            <div className={classes.row}>
                <div className={classes.playerContainer}>
                    <Player />

                    <div className={classes.syncIndicator}>
                        <SyncIndicator />
                    </div>
                </div>

                <ClientList />
            </div>

            <div className={classes.controls}>
                <SyncGroupControls />

                <SyncControls />

                <SyncStats />
            </div>
        </div>
    );
};
export default PlayerPage;
