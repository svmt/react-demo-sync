import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
    pageContainer: {
        color: '#fff',
    },
    row: {
        display: 'flex',
        justifyContent: 'center',
    },
    playerContainer: {
        width: 960,
        height: 540,
        display: 'flex',
        justifyContent: 'center',
        position: 'relative',
    },
    controls: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    syncIndicator: {
        position: 'absolute',
        right: 10,
        zIndex: 1,
        top: 10,
    },
});
