import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
    dataInfo: {
        padding: 20,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        border: '2px solid #ffffff',
    },
    typoContainer: {
        display: 'flex',
        alignItems: 'center'
    },
    typoLabel: {
        marginRight: 5,
        color: '#ffffff',
        fontFamily: 'Arial,Helvetica,sans-serif',
    },
    typoValue: {
        color: '#fff',
        fontSize: 18,
    },
});
