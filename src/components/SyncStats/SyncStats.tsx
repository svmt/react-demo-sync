import React, { useEffect, useState } from 'react';
import { Typography } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { on, off } from '../../services/PubSub';
import styles from './SyncStats.css';

const SyncStats: React.FC = () => {
    const classes = styles();
    const [accuracy, setAccuracy] = useState(0);
    const [delta, setDelta] = useState(0);
    const [rate, setRate] = useState(1);
    const { room } = useSelector((state: any) => state.main);

    function handleDeltaChange(v: number) {
        setDelta(Math.round(v));
    }

    function handleAccuracyChange(v: number) {
        setAccuracy(v);
    }

    function handlePlaybackRateChange(v: number) {
        setRate(v);
    }

    useEffect(() => {
        on('delta_change', handleDeltaChange);
        on('sync_accuracy_change', handleAccuracyChange);
        on('speed_change', handlePlaybackRateChange);

        return () => {
            off('delta_change', handleDeltaChange);
            off('sync_accuracy_change', handleAccuracyChange);
            off('speed_change', handlePlaybackRateChange);
        }
    }, [])

    return (
        <div className={classes.dataInfo}>
            <div className={classes.typoContainer}>
                <Typography className={classes.typoLabel}>Group Name:</Typography>
                <Typography className={classes.typoValue}>{room.groupName}</Typography>
            </div>
            <div className={classes.typoContainer}>
                <Typography className={classes.typoLabel}>Client Name:</Typography>
                <Typography className={classes.typoValue}>{room.clientName}</Typography>
            </div>
            <div className={classes.typoContainer}>
                <Typography className={classes.typoLabel}>Delta:</Typography>
                <Typography className={classes.typoValue}>{delta} ms</Typography>
            </div>
            <div className={classes.typoContainer}>
                <Typography className={classes.typoLabel}>Sync accuracy:</Typography>
                <Typography className={classes.typoValue}>{accuracy}%</Typography>
            </div>
            <div className={classes.typoContainer}>
                <Typography className={classes.typoLabel}> Playback speed:</Typography>
                <Typography className={classes.typoValue}>{Number(rate).toFixed(3)}</Typography>
            </div>
        </div>
    )
}

export default SyncStats
