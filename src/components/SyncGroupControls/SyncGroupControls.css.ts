import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
    root: {
        textAlign: 'center',
        marginTop: 10
    },
    sliderContainer: {
        width: 100,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    playerControl: {
        display: 'flex',
        flexDirection: 'row',
        marginLeft: 10,
        marginRight: 10,
        borderWidth: 1,
        borderColor: '#fff',
        borderStyle: 'solid',
        padding: 5,
        margin: 5,
    },
    playerControlButton: {
        width: 50,
        color: '#fff',
    },
    label: {
        color: '#ffffff',
        fontFamily: 'Arial,Helvetica,sans-serif',
    }
});
