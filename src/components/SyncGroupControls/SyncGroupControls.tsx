import React, { useEffect, useState } from 'react';
import { IconButton, Slider, Typography } from '@material-ui/core';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Pause from '@material-ui/icons/Pause';
import VolumeOffIcon from '@material-ui/icons/VolumeOff';
import VolumeUp from '@material-ui/icons/VolumeUp';

import { muteGroup, pauseGroup, playGroup, setVolume, groupSeek, unmuteGroup } from '../../services/SynchronizeService';
import { publish, on, off } from '../../services/PubSub';
import styles from './SyncGroupControls.css';

const SyncGroupControls: React.FC = () => {
    const classes = styles();
    const [volumeValue, setVolumeValue] = useState<any>(0.05);
    const [muted, setMuted] = useState<boolean>(false);

    useEffect(() => {
        const listener = (event: { type: string; msg: any }) => {
            if (event.type === 'group_volume_change') {
                setVolumeValue(event.msg.volume);
            } else if (event.type === 'group_mute') {
                setMuted(true);
            } else if (event.type === 'group_unmute') {
                setMuted(false);
            }
        };

        on('remote_event', listener);

        return () => {
            off('remote_event', listener);
        };
    }, []);

    function handleGroupPLay() {
        publish('group_play');
        playGroup();
    }

    function handleGroupPause() {
        publish('group_pause');
        pauseGroup();
    }

    function handleGroupMute() {
        setMuted(true);
        publish('group_mute');
        muteGroup();
    }

    function handleGroupUnmute() {
        setMuted(false);
        publish('group_unmute');
        unmuteGroup();
    }

    function handleGroupVolume(volume: any) {
        setVolumeValue(volume);
        publish('group_volume', volume);
        setVolume(volume);
    }

    function handleGroupSeek(delta: number) {
        // group seek should be first, so that the current position has not changed yet
        groupSeek(delta);
        publish('group_seek', delta);
    }

    return (
        <div className={classes.root}>
            <Typography className={classes.label}>Group controls:</Typography>
            <div className={classes.playerControl} id="continuous-slider">
                <IconButton
                    aria-label="Play"
                    className={classes.playerControlButton}
                    size="small"
                    onClick={handleGroupPLay}
                >
                    <PlayArrow fontSize="inherit" />
                </IconButton>
                <IconButton
                    aria-label="Pause"
                    size="small"
                    onClick={handleGroupPause}
                    className={classes.playerControlButton}
                >
                    <Pause fontSize="inherit" />
                </IconButton>
                {muted ? (
                    <IconButton
                        aria-label="Mute"
                        size="small"
                        onClick={() => {
                            handleGroupUnmute();
                        }}
                        className={classes.playerControlButton}
                    >
                        <VolumeOffIcon fontSize="inherit" />
                    </IconButton>
                ) : (
                    <IconButton
                        aria-label="Mute"
                        size="small"
                        onClick={() => {
                            handleGroupMute();
                        }}
                        className={classes.playerControlButton}
                    >
                        <VolumeUp fontSize="inherit" />
                    </IconButton>
                )}
                <div className={classes.sliderContainer}>
                    <Slider
                        style={{ width: 100, color: '#fff' }}
                        value={volumeValue}
                        step={0.1}
                        max={1}
                        min={0}
                        onChange={(e, v) => handleGroupVolume(v)}
                        aria-labelledby="continuous-slider"
                    />
                </div>
            </div>

            <IconButton onClick={() => handleGroupSeek(-60000)}> - minute </IconButton>
            <IconButton onClick={() => handleGroupSeek(+60000)}> + minute </IconButton>
        </div>
    );
};

export default SyncGroupControls;
