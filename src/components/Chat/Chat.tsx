import React, { useEffect, useState } from 'react';
import { IconButton, Tooltip } from '@material-ui/core';
import MessageIcon from '@material-ui/icons/Message';
import { off, on } from '../../services/PubSub';
import ChatDialog from './ChatDialog/ChatDialog';
import styles from './Chat.css';

const Chat: React.FC = () => {
    const s = styles();
    const [isChatVisible, setChatVisible] = useState<boolean>(false);
    const [messages, setMessages] = useState([]);

    function handleMessage(message: any) {
        // @ts-ignore
        setMessages((current: any) => [...current, { message, date: new Date().toISOString().slice(11, 16).toString() }]);
    }

    useEffect(() => {
        on('chat_update', handleMessage);

        return () => {
            off('chat_update', handleMessage);
        };
    }, []);

    const openChat = () => {
        setChatVisible(!isChatVisible);
    };

    return (
        <div className={s.chatContainer}>
            {isChatVisible && (
                <div className={s.chatDialog}>
                    <ChatDialog chatClose={openChat} messages={messages} />
                </div>
            )}
            <Tooltip title={<div className={s.tooltipBox}>Group chat</div>}>
                <IconButton aria-label="Open" className={s.openChatButton} size="small" onClick={() => openChat()}>
                    <MessageIcon fontSize="inherit" />
                </IconButton>
            </Tooltip>
        </div>
    );
};

export default Chat;
