import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
    chatContainer: {
        color: '#fff'
    },
    openChatButton:{
        color: '#fff',
        width: 50,
        height: 50,
    },
    chatDialog: {
        position: 'fixed',
        right: '5%',
        top: '5%',
        zIndex: 99,
    },
    tooltipBox:{

    }
});
