import React, { useEffect, useState } from 'react';
import { off, on } from '../../services/PubSub';
import styles from './ClientList.css';

const ClientList = () => {
    const s = styles();
    const [clients, setClients] = useState([]);

    function handleClientListUpdate(result: any) {
        setClients(result.clientList);

        const leader  = result.clientList.find((a: any) => a.leader);
        console.log('Leader', leader)
    }

    useEffect(() => {
        on('client_list_update', handleClientListUpdate);

        return () => {
            off('client_list_update', handleClientListUpdate);
        }
    }, [])

    return (
        <div className={s.clientListContainer}>
            <div className={s.clientBlockTitle}>Clients in a group:</div>
            {clients.map((clientItem: any) => (
                <div className={s.clientItem} key={clientItem.name + clientItem.id}>
                    {' '}
                    Name: {clientItem.name}
                </div>
            ))}
        </div>
    );
};
export default ClientList;
