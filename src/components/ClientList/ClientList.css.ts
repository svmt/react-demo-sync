import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
    clientListContainer: {
        color: '#fff',
        paddingLeft: 20,
        paddingRight: 20,
        height: 540,
        width: 140,
        backgroundColor: '#1e222a',
    },
    clientItem: {
        padding: 10,
        width: '80%',
        margin: 2,
        borderRadius: 4,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#fff',
        marginTop: 10
    },
    clientBlockTitle:{
        color: '#fff',
        fontWeight: 600,
        fontSize: 16,
        paddingTop: 10,
    }
});
