import React, { useEffect, useState } from 'react';
import { Tooltip } from '@material-ui/core';
import { off, on } from '../../services/PubSub';
import styles from './SyncIndicator.css';

const SyncIndicator: React.FC = () => {
    const s = styles();
    const [accuracy, setAccuracy] = useState(0);

    function handleAccuracyChange(v: number) {
        setAccuracy(v);
    }

    useEffect(() => {
        on('sync_accuracy_change', handleAccuracyChange);

        return () => {
            off('sync_accuracy_change', handleAccuracyChange);
        }
    }, [])

    const getStyle = (syncPercent: number) => {
        if (syncPercent < 40) {
            return s.notSync;
        } else if (syncPercent > 40 && syncPercent < 55) {
            return s.middleOfSync;
        } else if (syncPercent > 55 && syncPercent < 80) {
            return s.almostSync;
        } else {
            return s.fullSync;
        }
    };

    return (
        <Tooltip title={<div className={s.tooltipBox}>Synchronization indicator</div>}>
            <div className={getStyle(accuracy)}/>
        </Tooltip>
    );
};
export default SyncIndicator;
