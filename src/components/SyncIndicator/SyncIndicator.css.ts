import { makeStyles } from '@material-ui/core/styles';

const SIZE = 15;

export default makeStyles({
    fullSync: {
        color: 'green',
        borderWidth: 1,
        borderColor: 'white',
        background: 'green',
        borderStyle: 'solid',
        textShadow: '1px 1px 2px black, 0 0 25px blue, 0 0 5px darkblue',
        borderRadius: SIZE,
        width: SIZE,
        height: SIZE,
    },
    notSync: {
        color: 'red',
        borderWidth: 1,
        borderColor: 'white',
        background: 'red',
        borderStyle: 'solid',
        borderRadius: SIZE,
        width: SIZE,
        height: SIZE,
    },
    almostSync: {
        color: 'yellow',
        borderWidth: 1,
        borderColor: 'white',
        background: 'yellow',
        borderStyle: 'solid',
        borderRadius: SIZE,
        width: SIZE,
        height: SIZE,
    },
    middleOfSync: {
        color: 'orange',
        borderWidth: 1,
        borderColor: 'white',
        background: 'orange',
        borderStyle: 'solid',
        borderRadius: SIZE,
        width: SIZE,
        height: SIZE,
    },
    tooltipBox: {
        padding: 3,
        fontSize: 11,
    },
});
