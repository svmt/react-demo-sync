import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import {
    startSynchronize, stopSynchronize,
} from '../../services/SynchronizeService';
import Chat from '../Chat/Chat';


const SyncControls: React.FC = () => {
    const history = useHistory();
    const [isSyncing, setIsSyncing] = useState(false);

    return (
        <div style={{ display: 'flex' }}>
            <button
                className="watch-together-button"
                disabled={isSyncing}
                style={{
                    margin: 10,
                    borderWidth: 2,
                    borderRadius: 0,
                }}
                onClick={() => {
                    startSynchronize();
                    setIsSyncing(true);
                }}
            >
                start sync
            </button>
            <button
                className="watch-together-button"
                disabled={!isSyncing}
                style={{
                    margin: 10,
                    borderWidth: 2,
                    borderRadius: 0,
                }}
                onClick={() => {
                    stopSynchronize();
                    setIsSyncing(false);
                }}
            >
                stop sync
            </button>
            <button
                className="watch-together-button leave"
                onClick={() => {
                    stopSynchronize();
                    // @ts-ignore
                    history.replace('/');
                }}
            >
                Leave Room
            </button>

            <Chat />
        </div>
    )
}

export default SyncControls
