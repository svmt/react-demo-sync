import React, { useCallback, useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import YTLoader from 'youtube-iframe';
import { setYoutubeToSdk } from '../../services/SynchronizeService';
import { off, on } from '../../services/PubSub';

const YoutubePlayer: React.FC = () => {
    const cleanup = useRef(() => {});
    const { room } = useSelector((state: any) => state.main);

    const handleReady = useCallback((player) => {
        if (!player) {
            return;
        }

        player.playVideo();
        setYoutubeToSdk(player);

        player.setVolume(5);

        const handleGroupPause = () => player.pauseVideo();
        const handleGroupPlay = () => player.playVideo();
        const handleGroupMute = () => player.mute();
        const handleGroupUnmute = () => player.unMute();
        const handleGroupVolume = (volume: number) => player.setVolume(volume * 100);
        const handleGroupSeek = (delta: number) => player.seekTo(Math.max(player.getCurrentTime() + delta / 1000, 0));

        on('group_pause', handleGroupPause);
        on('group_play', handleGroupPlay);
        on('group_mute', handleGroupMute);
        on('group_unmute', handleGroupUnmute);
        on('group_volume', handleGroupVolume);
        on('group_seek', handleGroupSeek);

        cleanup.current = () => {
            player.destroy();

            off('group_pause', handleGroupPause);
            off('group_play', handleGroupPlay);
            off('group_mute', handleGroupMute);
            off('group_unmute', handleGroupUnmute);
            off('group_volume', handleGroupVolume);
            off('group_seek', handleGroupSeek);
        }
    }, [])

    useEffect(() => {
        YTLoader.load((YT) => {
            new YT.Player('youtube-player', {
                height: '100%',
                width: '100%',
                videoId: room.video,
                events: {
                    onReady: (result: any) => {
                        handleReady(result.target);
                    },
                },
                playerVars: {
                    autoplay: true,
                    rel: 0,
                    showinfo: 0,
                    ecver: 2,
                    playsinline: 1,
                    modestbranding: false,
                },
            });
        });

        return () => {
            cleanup.current();
        }
    }, [room.video, handleReady]);

    return <div id="youtube-player" />;
};
export default YoutubePlayer;
