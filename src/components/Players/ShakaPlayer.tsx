import React, { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import 'shaka-player/dist/controls.css';
import { setShakaToSDK } from '../../services/SynchronizeService';
import { off, on } from '../../services/PubSub';
const shaka = require('shaka-player/dist/shaka-player.ui.js');

const ShakaPlayer = () => {
    const { room } = useSelector((state: any) => state.main);
    const uiContainerRef = useRef(null);
    const videoRef = useRef(null);

    useEffect(() => {
        if (!videoRef.current) {
            return () => {};
        }

        const video = videoRef.current as any;

        const player = new shaka.Player(video);
        const ui = new shaka.ui.Overlay(player, uiContainerRef.current, videoRef.current);

        player.load(room.video).then(() => {
            setShakaToSDK(player);
        });

        const handleGroupPause = () => video.pause();
        const handleGroupPlay = () => video.play();
        const handleGroupMute = () => (video.muted = true);
        const handleGroupUnmute = () => (video.muted = false);
        const handleGroupVolume = (volume: number) => (video.volume = volume);
        const handleGroupSeek = (delta: number) => (video.currentTime = Math.max(video.currentTime + delta / 1000, 0));

        on('group_pause', handleGroupPause);
        on('group_play', handleGroupPlay);
        on('group_mute', handleGroupMute);
        on('group_unmute', handleGroupUnmute);
        on('group_volume', handleGroupVolume);
        on('group_seek', handleGroupSeek);

        return () => {
            player.destroy();
            ui.destroy();

            off('group_pause', handleGroupPause);
            off('group_play', handleGroupPlay);
            off('group_mute', handleGroupMute);
            off('group_unmute', handleGroupUnmute);
            off('group_volume', handleGroupVolume);
            off('group_seek', handleGroupSeek);
        };
    }, [room.video]);

    return (
        <div
            ref={uiContainerRef}
            style={{
                maxWidth: '100%',
                width: '100%',
            }}
        >
            <video
                ref={videoRef}
                style={{
                    maxWidth: '100%',
                    width: '100%',
                }}
                autoPlay
            />
        </div>
    );
};

export default ShakaPlayer;
