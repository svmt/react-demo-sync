import React, { useEffect } from 'react';
import * as THEOplayer from 'theoplayer';
import { useSelector } from 'react-redux';
import { setTheoToSdk } from '../../services/SynchronizeService';
import { off, on } from '../../services/PubSub';
import 'theoplayer/ui.css'

const TheoPlayer = () => {
    const { room } = useSelector((state: any) => state.main);

    useEffect(() => {
        const config = {
            license: process.env.REACT_APP_THEO_LICENSE as string,
            libraryLocation: 'https://cdn.jsdelivr.net/npm/theoplayer@2.92.0',
            ui : {
                width: '960px',
                height: '540px'
            },
        };

        const player = new THEOplayer.Player(document.getElementById('theo-player-id') as HTMLElement, config);

        player.volume = 0.05;
        player.autoplay = true;

        player.source = {
            sources: [{
                src: room.video,
            }]
        };

        setTheoToSdk(player);

        const handleGroupPause = () => player.pause();
        const handleGroupPlay = () => player.play();
        const handleGroupMute = () => player.muted = true;
        const handleGroupUnmute = () => player.muted = false;
        const handleGroupVolume = (volume: number) => player.volume = volume;
        const handleGroupSeek = (delta: number) => player.currentTime = Math.max(player.currentTime +  delta / 1000, 0);

        on('group_pause', handleGroupPause);
        on('group_play', handleGroupPlay);
        on('group_mute', handleGroupMute);
        on('group_unmute', handleGroupUnmute);
        on('group_volume', handleGroupVolume);
        on('group_seek', handleGroupSeek);

        return () => {
            player.destroy();

            off('group_pause', handleGroupPause);
            off('group_play', handleGroupPlay);
            off('group_mute', handleGroupMute);
            off('group_unmute', handleGroupUnmute);
            off('group_volume', handleGroupVolume);
            off('group_seek', handleGroupSeek);
        }
    }, [room.video]);

    return <div id="theo-player-id" className="video-js theoplayer-skin"/>;
};
export default TheoPlayer;
