export function wrapperReq(request, drmToken) {
        // request.allowCrossSiteCredentials = true;
        var rawLicenseRequest = new Uint8Array(request.body);
        var wrapped = {};
        wrapped.rawLicenseRequestBase64 = btoa(String.fromCharCode.apply(null, rawLicenseRequest));
            wrapped.drmToken = drmToken;


        if (request.drm === 'com.apple.fps') {
            wrapped.rawLicenseRequestBase64 = request.body;
            wrapped.contentKID = request.keyIds[0];
            request.headers['content-type'] = 'application/x-www-form-urlencoded';
        }
        // Encode the wrapped request as JSON.
        var wrappedJson = JSON.stringify(wrapped);
        // Convert the JSON string back into a Uint8Array to replace the request body.
        request.body = new Uint8Array(wrappedJson.length);
        for (var i = 0; i < wrappedJson.length; ++i) {
            request.body[i] = wrappedJson.charCodeAt(i);
        }
}
