import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { setVOPlayerToSDK } from '../../../services/SynchronizeService';
import { off, on } from '../../../services/PubSub';
import { wrapperReq } from  './helper.js'



const VOPLayer = () => {
    const { room } = useSelector((store: any) => store.main)

    const initWithDrm = async (player: any) => {
        const result = await fetch('https://npx-webvtt2.duckdns.org/support/sceenic_live_drm.php?raw=1&device=a') as any;
        player.configure({ autostart: true,
            drm: {
                servers: {
                    "com.widevine.alpha": "https://npx-webvtt2.duckdns.org/drm/wrapperWV",
                    "com.apple.fps": "https://npx-webvtt2.duckdns.org/drm/wrapperFP"
                }
            },
        });

        const data = await result.json();

        player.unregisterLicenseRequestFilter((request: any) => wrapperReq(request, data.drmToken))
        player.registerLicenseRequestFilter((request: any) => wrapperReq(request, data.drmToken))

        player.on('error', console.warn);

        player.load(data.asset).then(() => {
            setVOPlayerToSDK(player);
            // player.play();
        });
    }

    useEffect(() => {
        // @ts-ignore
        const player = window.voplayer.createPlayerWithControls('vo-player', {
            base: '/voplayer',
            license: process.env.REACT_APP_VO_KEY,
        });


        if (!room.video) {
            initWithDrm(player);
        } else {
            player.configure({ autostart: true });
            player.load(room.video).then(() => {
                setVOPlayerToSDK(player);
            });
        }


        player.video_.volume = 0.05;

        const handleGroupPause = () => player.pause();
        const handleGroupPlay = () => player.play();
        const handleGroupMute = () => (player.video_.muted = true);
        const handleGroupUnmute = () => (player.video_.muted = false);
        const handleGroupVolume = (volume: number) => (player.video_.volume = volume);
        const handleGroupSeek = (delta: number) => (player.video_.currentTime = Math.max(player.currentTime + delta / 1000, 0));

        on('group_pause', handleGroupPause);
        on('group_play', handleGroupPlay);
        on('group_mute', handleGroupMute);
        on('group_unmute', handleGroupUnmute);
        on('group_volume', handleGroupVolume);
        on('group_seek', handleGroupSeek);

        return () => {
            player.destroy();

            off('group_pause', handleGroupPause);
            off('group_play', handleGroupPlay);
            off('group_mute', handleGroupMute);
            off('group_unmute', handleGroupUnmute);
            off('group_volume', handleGroupVolume);
            off('group_seek', handleGroupSeek);
        }
    }, [room.video])

    return <div id="vo-player" style={{ width: '100%', height: '100%' }}/>
}
export default VOPLayer
