import React, { useEffect } from 'react';
// @ts-ignore
import HLSPlugin from '@flowplayer/player/plugins/hls';
// @ts-ignore
import DASHPlugin from '@flowplayer/player/plugins/dash';
import Player from '@flowplayer/player';
import { useSelector } from 'react-redux';
import { setFlowPlayerToSdk } from '../../services/SynchronizeService';
import { off, on } from '../../services/PubSub';

import '@flowplayer/player/flowplayer.css';

const FlowPlayer = () => {
    const { room } = useSelector((store: any) => store.main);

    useEffect(() => {
        if (room.video.includes('.mpd')) {
            Player(DASHPlugin);
        } else {
            Player(HLSPlugin);
        }

        // @ts-ignore
        const player = Player(`#flow-player`, {
            autoplay: true,
            token: process.env.REACT_APP_FLOW_TOKEN,
            src: room.video,
            // hls: { native: true },
            ratio: '16:9',
        });

        setFlowPlayerToSdk(player);

        player.volume = 0.05;
        player.play();

        const handleGroupPause = () => player.pause();
        const handleGroupPlay = () => player.play();
        const handleGroupMute = () => (player.muted = true);
        const handleGroupUnmute = () => (player.muted = false);
        const handleGroupVolume = (volume: number) => (player.volume = volume);
        const handleGroupSeek = (delta: number) =>
            (player.currentTime = Math.max(player.currentTime + delta / 1000, 0));

        on('group_pause', handleGroupPause);
        on('group_play', handleGroupPlay);
        on('group_mute', handleGroupMute);
        on('group_unmute', handleGroupUnmute);
        on('group_volume', handleGroupVolume);
        on('group_seek', handleGroupSeek);

        return () => {
            console.log('dest');
            // @ts-ignore
            player.dash?.destroy();
            player.hls?.destroy();
            player.destroy();

            off('group_pause', handleGroupPause);
            off('group_play', handleGroupPlay);
            off('group_mute', handleGroupMute);
            off('group_unmute', handleGroupUnmute);
            off('group_volume', handleGroupVolume);
            off('group_seek', handleGroupSeek);
        };
    }, [room.video]);

    return <div id="flow-player" />;
};

export default FlowPlayer;
