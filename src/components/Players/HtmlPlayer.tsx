import React, { useEffect } from 'react';
import Hls from 'hls.js';
import { useSelector } from 'react-redux';
import { off, on } from '../../services/PubSub';
import { setHtmlPlayerToSdk } from '../../services/SynchronizeService';

const HtmlPlayer: React.FC = () => {
    const { room } = useSelector((store: any) => store.main);

    useEffect(() => {
        const video = document.getElementById('html-player') as HTMLVideoElement;
        const hls = new Hls({});
        hls.attachMedia(video);

        hls.once(Hls.Events.MEDIA_ATTACHED, function () {
            hls.loadSource(room.video);

            hls.once(Hls.Events.LEVEL_LOADED, function () {
                setHtmlPlayerToSdk({ video, hls });
            });
        });

        video.volume = 0.05;

        const handleGroupPause = () => video.pause();
        const handleGroupPlay = () => video.play();
        const handleGroupMute = () => (video.muted = true);
        const handleGroupUnmute = () => (video.muted = false);
        const handleGroupVolume = (volume: number) => (video.volume = volume);
        const handleGroupSeek = (delta: number) => (video.currentTime = Math.max(video.currentTime + delta / 1000, 0));

        on('group_pause', handleGroupPause);
        on('group_play', handleGroupPlay);
        on('group_mute', handleGroupMute);
        on('group_unmute', handleGroupUnmute);
        on('group_volume', handleGroupVolume);
        on('group_seek', handleGroupSeek);

        return () => {
            hls.destroy();

            off('group_pause', handleGroupPause);
            off('group_play', handleGroupPlay);
            off('group_mute', handleGroupMute);
            off('group_unmute', handleGroupUnmute);
            off('group_volume', handleGroupVolume);
            off('group_seek', handleGroupSeek);
        };
    }, [room.video]);

    return <video id="html-player" width="100%" height="100%" controls={true} autoPlay playsInline/>;
};
export default HtmlPlayer;
