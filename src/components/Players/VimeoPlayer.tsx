import React, { useEffect } from 'react';
// @ts-ignore
import Vimeo from '@vimeo/player';
import { useSelector } from 'react-redux';
import { off, on } from '../../services/PubSub';
import { setVimeoToSdk } from '../../services/SynchronizeService';

const VimeoPlayer = () => {
    const { room } = useSelector((state: any) => state.main);

    useEffect(() => {
        const player = new Vimeo('vimeo-player-id', {id: room.video, width: 960,  height: 540})

        setVimeoToSdk(player);

        player.setVolume(0.05);
        player.play();

        const handleGroupPause = () => player.pause();
        const handleGroupPlay = () => player.play();
        const handleGroupMute = () => player.setMuted(true);
        const handleGroupUnmute = () => player.setMuted(true);
        const handleGroupVolume = (volume: number) => player.setVolume(volume);
        const handleGroupSeek = async (delta: number) => {
            const currentTime = await player.getCurrentTime();
            player.setCurrentTime(Math.max(currentTime +  delta / 1000, 0))
        };

        on('group_pause', handleGroupPause);
        on('group_play', handleGroupPlay);
        on('group_mute', handleGroupMute);
        on('group_unmute', handleGroupUnmute);
        on('group_volume', handleGroupVolume);
        on('group_seek', handleGroupSeek);

        return () => {
            player.destroy();

            off('group_pause', handleGroupPause);
            off('group_play', handleGroupPlay);
            off('group_mute', handleGroupMute);
            off('group_unmute', handleGroupUnmute);
            off('group_volume', handleGroupVolume);
            off('group_seek', handleGroupSeek);
        }
    }, [room.video]);

    return <div id="vimeo-player-id" />;
};
export default VimeoPlayer;
