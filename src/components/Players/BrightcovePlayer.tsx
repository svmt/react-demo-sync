import React, { useEffect, useState } from 'react';
// @ts-ignore
import ReactPlayerLoader from '@brightcove/react-player-loader';
import { useSelector } from 'react-redux';
import { setBrightcoveToSdk } from '../../services/SynchronizeService';
import { off, on } from '../../services/PubSub';

const BrightcovePlayer = () => {
    const [player, setPlayer] = useState<any>();
    const { room } = useSelector((store: any) => store.main);

    useEffect(() => {
        if (!player) {
            return;
        }

        player.volume(0.05)

        const handleGroupPause = () => player.pause();
        const handleGroupPlay = () => player.play();
        const handleGroupMute = () => player.muted(true);
        const handleGroupUnmute = () => player.muted(false);
        const handleGroupVolume = (volume: number) => player.volume(volume);
        const handleGroupSeek = (delta: number) => player.currentTime(Math.max(player.currentTime() + delta / 1000, 0));

        on('group_pause', handleGroupPause);
        on('group_play', handleGroupPlay);
        on('group_mute', handleGroupMute);
        on('group_unmute', handleGroupUnmute);
        on('group_volume', handleGroupVolume);
        on('group_seek', handleGroupSeek);

        return () => {
            player.dispose();

            off('group_pause', handleGroupPause);
            off('group_play', handleGroupPlay);
            off('group_mute', handleGroupMute);
            off('group_unmute', handleGroupUnmute);
            off('group_volume', handleGroupVolume);
            off('group_seek', handleGroupSeek);
        };
    }, [player]);

    const onSuccess = async (success: any) => {
        const player = success.ref;
        setPlayer(player);

        player.on('loadeddata', () => {
            setBrightcoveToSdk(player);
        })
    };

    return (
        <ReactPlayerLoader
            width="100%"
            height="100%"
            accountId={process.env.REACT_APP_BRIGHTCOVE_ACCOUNT}
            onSuccess={onSuccess}
            videoId={room.video}
        />
    );
};

export default BrightcovePlayer;
