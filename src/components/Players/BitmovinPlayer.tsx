import React, { useEffect } from 'react';
import { Player } from 'bitmovin-player';
import { useSelector } from 'react-redux';
import { isiOS } from '../../helper/device';
import { setBitmovinToSdk } from '../../services/SynchronizeService';
import { off, on } from '../../services/PubSub';

const BitmovinPlayer = () => {
    const { room } = useSelector((state: any) => state.main);

    useEffect(() => {
        const config = {
            key: process.env.REACT_APP_BITMOVIN_KEY as string,
            location: {
                ui: 'https://cdn.bitmovin.com/player/web/8/bitmovinplayer-ui.js',
                ui_css: 'https://cdn.bitmovin.com/player/web/8/bitmovinplayer-ui.css',
            },
            playback: {
                volume: 5,
            },
        };

        const player = new Player(document.getElementById('bitmovin-player-id') as HTMLElement, config);

        const src = {
            hls: undefined,
            dash: undefined,
        };

        if (room.video.includes('.mpd')) {
            src.dash = room.video;
        } else {
            src.hls = room.video;
        }

        player.load(src).then(() => {
            setBitmovinToSdk(player);

            if (!isiOS()) player.play();
        });

        const handleGroupPause = () => player.pause();
        const handleGroupPlay = () => player.play();
        const handleGroupMute = () => player.mute();
        const handleGroupUnmute = () => player.unmute();
        const handleGroupVolume = (volume: number) => player.setVolume(volume * 100);
        const handleGroupSeek = (delta: number) => player.seek(Math.max(player.getCurrentTime() + delta / 1000, 0));

        on('group_pause', handleGroupPause);
        on('group_play', handleGroupPlay);
        on('group_mute', handleGroupMute);
        on('group_unmute', handleGroupUnmute);
        on('group_volume', handleGroupVolume);
        on('group_seek', handleGroupSeek);

        return () => {
            player.unload();

            off('group_pause', handleGroupPause);
            off('group_play', handleGroupPlay);
            off('group_mute', handleGroupMute);
            off('group_unmute', handleGroupUnmute);
            off('group_volume', handleGroupVolume);
            off('group_seek', handleGroupSeek);
        };
    }, [room.video]);

    return <div id="bitmovin-player-id" />;
};
export default BitmovinPlayer;
