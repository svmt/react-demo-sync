import React, { useCallback, useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { setNexPlayerToSdk } from '../../services/SynchronizeService';
import { off, on } from '../../services/PubSub';
import { loadScript } from '../../helper/loadScript';

const NexPlayer = () => {
    const cleanup = useRef(() => {});
    const { room } = useSelector((store: any) => store.main);

    const handleReady = useCallback((player) => {
        if (!player) {
            return;
        }

        setNexPlayerToSdk(player);

        player.setVolume(0.05);

        let volumeBeforeMute = 0;

        const handleGroupPause = () => {
            if (player.isPlaying()) player.togglePlayPause();
        };
        const handleGroupPlay = () => {
            if (!player.isPlaying()) player.togglePlayPause();
        };
        const handleGroupMute = () => {
            volumeBeforeMute = player.getVolume();
            player.setVolume(0);
        }
        const handleGroupUnmute = () => {
            player.setVolume(volumeBeforeMute);
            volumeBeforeMute = 0;
        }
        const handleGroupVolume = (volume: number) => player.setVolume(volume);
        const handleGroupSeek = (delta: number) => player.seek(Math.max(player.getCurrentTime() + delta / 1000, 0));

        on('group_pause', handleGroupPause);
        on('group_play', handleGroupPlay);
        on('group_mute', handleGroupMute);
        on('group_unmute', handleGroupUnmute);
        on('group_volume', handleGroupVolume);
        on('group_seek', handleGroupSeek);

        cleanup.current = () => {
            player.destroy();

            off('group_pause', handleGroupPause);
            off('group_play', handleGroupPlay);
            off('group_mute', handleGroupMute);
            off('group_unmute', handleGroupUnmute);
            off('group_volume', handleGroupVolume);
            off('group_seek', handleGroupSeek);
        }
    }, [])

    useEffect(() => {
        // @ts-ignore
        window.loadInterval = null;
        // @ts-ignore
        window.player = null;
        loadScript('https://nexplayer.nexplayersdk.com/7.1.1/nexplayer.js', () => {
            // @ts-ignore
            window.nexplayer.Setup({
                key: process.env.REACT_APP_NEX_KEY,
                div: document.getElementById('nex-player'),
                debug: false,
                seekUI: 0,
                // lowLatency: true,
                autoplay: true,
                src: room.video,
                callbacksForPlayer: (player: any) => {
                    handleReady(player.player_);
                },
            });
        });

        return () => {
            cleanup.current();
        }
    }, [room.video, handleReady]);

    return <div id="nex-player" style={{ width: '100%', height: '100%' }} />;
};

export default NexPlayer;
