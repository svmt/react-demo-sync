import * as actionTypes from '../actionTypes'

const initialState = {
    room: null,
};

const main = (
    state: any = initialState,
    action: any
): any => {
    switch (action.type) {
        case actionTypes.SET_ROOM:
            return {
                ...state,
                room: action.payload,
            };
    }
    return state
};

export default main
