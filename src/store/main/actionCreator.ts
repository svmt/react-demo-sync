import * as actionTypes from './actionTypes';

export const setRoom =
    (roomInfo: { groupName: string; clientName: string; video: string }) => async (dispatch: any) => {
        const action = {
            type: actionTypes.SET_ROOM,
            payload: roomInfo,
        };
        dispatch(action);
    };
