import { createStore, applyMiddleware, Store, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import main from './main/reducers/main';
import thunk from 'redux-thunk';

const reducers = combineReducers({
    main,
});

const store: Store<any, any> & {
    dispatch: any;
} = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));

export default store;
