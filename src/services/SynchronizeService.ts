import axios from 'axios';
import axiosRetry from 'axios-retry';
import { SyncSDK, Callbacks, LogLevels } from '@sscale/syncsdk';
import { DemoYoutubePlayer } from '../decorators/DemoYoutubePlayer';
import { DemoHtmlPlayer } from '../decorators/DemoHtmlPlayer';
import { DemoVimeoPlayer } from '../decorators/DemoVimeoPlayer';
import { DemoShakaPlayer } from '../decorators/DemoShakaPlayer';
import { DemoVOPlayer } from '../decorators/DemoVOPlayer';
import { DemoBitmovinPlayer } from '../decorators/DemoBitmovinPlayer';
import { DemoTheoPlayer } from '../decorators/DemoTheoPlayer';
import { DemoFlowPlayer } from '../decorators/DemoFlowPlayer';
import { DemoNexPlayer } from '../decorators/DemoNexPlayer';
import { DemoBrightcovePlayer } from '../decorators/DemoBrightcovePlayer';
import { publish } from './PubSub';

axiosRetry(axios, {
    retries: 3,
    retryCondition: (error) => {
        return error.response?.status === 500;
    },
});

let syncInstance: any;

export const initializeSyncSdk = () => {
    if (syncInstance) {
        syncInstance.removeListener(Callbacks.delta_change);
        syncInstance.removeListener(Callbacks.sync_accuracy_change);
        syncInstance.removeListener(Callbacks.speed_change);
        syncInstance.removeListener(Callbacks.client_list_update);
        syncInstance.removeListener(Callbacks.error);
        syncInstance.removeListener(Callbacks.chat_update);
        syncInstance.removeListener(Callbacks.remote_event);
    }

    syncInstance = new SyncSDK();

    syncInstance.setLogLevel(LogLevels.DEBUG);

    syncInstance.attachListener(publish.bind(null, 'delta_change'), Callbacks.delta_change);
    syncInstance.attachListener(publish.bind(null, 'sync_accuracy_change'), Callbacks.sync_accuracy_change);
    syncInstance.attachListener(publish.bind(null, 'speed_change'), Callbacks.speed_change);
    syncInstance.attachListener(publish.bind(null, 'client_list_update'), Callbacks.client_list_update);
    syncInstance.attachListener(publish.bind(null, 'error'), Callbacks.error);
    syncInstance.attachListener(publish.bind(null, 'chat_update'), Callbacks.chat_update);
    syncInstance.attachListener(publish.bind(null, 'remote_event'), Callbacks.remote_event);
};

// @ts-ignore
export const createGroup = async ({ token, groupName, clientName }) => {
    if (!token) {
        // This is for demo purposes only!!!
        const { data } = await axios.post(`https://sync-share.s1.sceenic.co/get-token?room_name=${groupName}`, {
            token: process.env.REACT_APP_TOKEN,
        });
        token = data.jwt_token;
    }

    await syncInstance.createGroup(token, clientName);
};

export const setHtmlPlayerToSdk = (player: any) => {
    console.log('html', player);
    // @ts-ignore
    window.html = player;
    syncInstance.addPlayerClient(new DemoHtmlPlayer(player));
};

export const setYoutubeToSdk = (player: any) => {
    console.log('youtube', player);
    // @ts-ignore
    window.youtube = player;
    syncInstance.addPlayerClient(new DemoYoutubePlayer(player));
};

export const setVimeoToSdk = (player: any) => {
    console.log('vimeo', player);
    // @ts-ignore
    window.vimeo = player;
    syncInstance.addPlayerClient(new DemoVimeoPlayer(player));
};

export const setShakaToSDK = (player: any) => {
    console.log('shaka', player);
    // @ts-ignore
    window.shaka = player;
    syncInstance.addPlayerClient(new DemoShakaPlayer(player));
};

export const setVOPlayerToSDK = (player: any) => {
    console.log('VO', player);
    // @ts-ignore
    window.VO = player;
    syncInstance.addPlayerClient(new DemoVOPlayer(player));
};

export const setFlowPlayerToSdk = (player: any) => {
    console.log('flow', player);
    // @ts-ignore
    window.flow = player;
    syncInstance.addPlayerClient(new DemoFlowPlayer(player));
};

export const setNexPlayerToSdk = (player: any) => {
    console.log('nex', player);
    // @ts-ignore
    window.nex = player;
    syncInstance.addPlayerClient(new DemoNexPlayer(player));
};

export const setBitmovinToSdk = (player: any) => {
    console.log('bitmovin', player);
    // @ts-ignore
    window.bitmovin = player;
    syncInstance.addPlayerClient(new DemoBitmovinPlayer(player));
};

export const setTheoToSdk = (player: any) => {
    console.log('theo', player);
    // @ts-ignore
    window.theo = player;
    syncInstance.addPlayerClient(new DemoTheoPlayer(player));
};

export const setBrightcoveToSdk = (player: any) => {
    console.log('brightcove', player);
    // @ts-ignore
    window.brightcove = player;
    syncInstance.addPlayerClient(new DemoBrightcovePlayer(player));
};

export const startSynchronize = () => {
    syncInstance.startSynchronize().catch((e: any) => console.log([e]));
};
export const stopSynchronize = () => {
    syncInstance?.stopSynchronize();
};

export const playGroup = () => {
    syncInstance?.groupPlay();
};
export const pauseGroup = () => {
    syncInstance?.groupPause();
};
export const setVolume = (volume: number) => {
    syncInstance?.groupChangeVolume(volume);
};
export const muteGroup = () => {
    syncInstance?.muteGroup();
};
export const unmuteGroup = () => {
    syncInstance?.unmuteGroup();
};
export const sendMessageToChat = (message: string) => {
    return syncInstance?.sendMessageToGroup(message);
};
export const groupSeek = (delta?: number) => {
    return syncInstance?.setGroupPosition(delta);
};
