import React from 'react';
import { Route, BrowserRouter as Router, Switch, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import CreatingRoom from '../pages/CreatingRoom/CreatingRoom';
import DemoList from '../pages/DemoList/DemoList';
import PlayerPage from '../pages/PlayerPage/PlayerPage';

const AppRouter = () => {
    const { room } = useSelector((state: any) => state.main);

    return (
        <div className="app-container">
            <div className="powered-by-container">
                <div className="powered-by-text">Powered by</div>
                <div className="logo-top"></div>
            </div>

            <Router>
                <Switch>
                    <Route path="/" exact={true}>
                        <DemoList />
                    </Route>
                    <Route path="/create-room" exact={true}>
                        <CreatingRoom />
                    </Route>
                    <Route path="/room/:player">
                        {!room ? (
                            <Redirect to="/" />
                        ) : (
                            <PlayerPage />
                        )}
                    </Route>
                </Switch>
            </Router>
        </div>
    );
};

export default AppRouter;
