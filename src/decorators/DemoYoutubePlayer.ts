import { PlayerDecorator } from '@sscale/syncsdk';
import { isSafariOriOS } from '../helper/device';

export class DemoYoutubePlayer extends PlayerDecorator {
    public getPrecisionThreshold() {
        // Improve experience with stuttering issue on iOS and Safari
        if (isSafariOriOS()) {
            return 350;
        }

        return 50;
    }

    public isSeekable() {
        return true;
    }

    public isStalled(): boolean {
        try {
            return this.player.getPlayerState() === 3;
        } catch (e) {
            console.error('Is stalled error:', e);
        }

        return false;
    }

    public play(): void {
        try {
            this.player.playVideo();
        } catch (e) {
            console.error('Play error:', e);
        }
    }

    public pause(): void {
        try {
            this.player.pauseVideo();
        } catch (e) {
            console.error('Pause error:', e);
        }
    }

    public getCurrentPosition(): number {
        try {
            return Math.round(this.player.getCurrentTime() * 1000);
        } catch (e) {
            console.error('Get current position error:', e);
        }

        return 0;
    }

    public getPlaybackRate(): number {
        try {
            return this.player.getPlaybackRate();
        } catch (e) {
            console.error('Get playback rate error:', e);
        }

        return 0;
    }

    public fastSeekToPosition(position: number) {
        try {
            this.player.seekTo(position / 1000, true);
        } catch (e) {
            console.error('Fast seek error:', e);
        }
    }

    public isPlaying(): boolean {
        try {
            return this.player.getPlayerState() === 1 || this.player.getPlayerState() === 3;
        } catch (e) {
            console.error('Is playing error:', e);
        }

        return false;
    }

    public mute(): void {
        try {
            this.player.mute();
        } catch (e) {
            console.error('Mute error:', e);
        }
    }

    public unmute(): void {
        try {
            this.player.unmute();
        } catch (e) {
            console.error('Unmute error:', e);
        }
    }

    public changePlaybackRate(rate: number): void {
        try {
            // Improve experience with stuttering issue on iOS and Safari
            if (isSafariOriOS()) {
                if (rate > 1) {
                    rate = Math.ceil(rate * 10) / 10 + 0.3;
                }

                if (Math.ceil(rate * 100) / 100 === Math.ceil(this.getPlaybackRate() * 100) / 100) {
                    return;
                }
            }

            this.player.setPlaybackRate(rate);
        } catch (e) {
            console.error('Set playback rate error:', e);
        }
    }

    public setVolume(volume: number) {
        try {
            this.player.setVolume(volume * 100);
        } catch (e) {
            console.error('Set volume error:', e);
        }
    }
}
