import { PlayerDecorator } from '@sscale/syncsdk';

export class DemoBrightcovePlayer extends PlayerDecorator {
    private streamOffset = 0;
    private rememberedPosition = 0;

    public load() {
        if (!this.isLive()) {
            return;
        }

        let tracks = this.player.textTracks();
        let segmentMetadataTrack: any;

        for (let i = 0; i < tracks.length; i++) {
            if (tracks[i].label === 'segment-metadata') {
                segmentMetadataTrack = tracks[i];
            }
        }

        if (segmentMetadataTrack) {
            const streamOffset = segmentMetadataTrack.activeCues[0]?.value?.dateTimeObject?.getTime();
            if (streamOffset) {
                this.streamOffset = streamOffset;
                this.rememberedPosition = this.player.currentTime();
            }

            segmentMetadataTrack.on('cuechange', () => {
                const streamOffset = segmentMetadataTrack.activeCues[0]?.value?.dateTimeObject?.getTime();
                if (streamOffset) {
                    this.streamOffset = streamOffset;
                    this.rememberedPosition = this.player.currentTime();

                }
            });
        }
    }

    private isLive() {
        return this.player.duration() === Infinity;
    }

    public getPrecisionThreshold() {
        if (this.isLive()) {
            return 75;
        }

        return 10;
    }

    public isSeekable(): boolean {
        return !this.isLive();
    }

    public isStalled(): boolean {
        return this.player.readyState() < 3 // HAVE_FUTURE_DATA;
    }

    public play(): void {
        try {
            this.player.play();
        } catch (e) {
            console.error('Play error:', e);
        }
    }

    public mute(): void {
        try {
            this.player.muted(true);
        } catch (e) {
            console.error('Mute error:', e);
        }
    }

    public unmute(): void {
        try {
            this.player.muted(false);
        } catch (e) {
            console.error('Unmute error:', e);
        }
    }

    public pause(): void {
        try {
            this.player.pause();
        } catch (e) {
            console.error('Pause error:', e);
        }
    }

    public getCurrentPosition(): number {
        try {
            if (this.isLive()) {
                if (!this.streamOffset) {
                    return 0;
                } else {
                    return Math.round(this.streamOffset + (this.player.currentTime() - this.rememberedPosition) * 1000);
                }
            } else {
                return Math.round(this.player.currentTime() * 1000);
            }
        } catch (e) {
            console.error('Get current position error:', e);
        }

        return 0;
    }

    public fastSeekToPosition(position: number) {
        if (position != null) {
            try {
                if (this.isLive()) {
                    if (!this.streamOffset) {
                        return;
                    } else {
                        const time = (position - this.streamOffset + this.rememberedPosition * 1000) / 1000;
                        this.player.currentTime(time / 1000);
                    }
                } else {
                    this.player.currentTime(position / 1000);
                }
            } catch (err) {
                console.error('Fast seek error: ', err);
            }
        }
    }

    public isPlaying(): boolean {
        try {
            return !this.player.paused();
        } catch (e) {
            console.error('Is playing error:', e);
        }

        return false;
    }

    public getPlaybackRate(): number {
        try {
            return this.player.playbackRate();
        } catch (e) {
            console.error('Get playback rate error:', e);
        }

        return 0;
    }

    public changePlaybackRate(rate: number): void {
        try {
            this.player.playbackRate(rate);
        } catch (e) {
            console.error('Set playback rate error:', e);
        }
    }

    public setVolume(volume: number) {
        try {
            this.player.volume(volume);
        } catch (e) {
            console.error('Set volume error:', e);
        }
    }
}
