import Hls from 'hls.js';
import { PlayerDecorator } from '@sscale/syncsdk';

export class DemoVOPlayer extends PlayerDecorator {
    private static DEFAULT_PTS_FREQUENCY = 90000;

    private streamOffset = 0;
    private rememberedPosition = 0;
    private usingProgramDateTime = false;
    private hls = false;

    public load() {
        if (!Hls.isSupported() || !this.player.isLive()) {
            return;
        }

        const hls = this.player.currentImplementation.hlsPlayer;

        if (!hls) {
            return;
        }

        this.hls = true;

        const initPTS = hls.timelineController.initPTS;
        if (initPTS[0]) {
            const streamOffset = (Math.abs(initPTS[0]) / DemoVOPlayer.DEFAULT_PTS_FREQUENCY) * 1000;
            this.streamOffset = Math.round(streamOffset);
        } else {
            hls.on(Hls.Events.INIT_PTS_FOUND, (event: any, eventData: any) => {
                if (eventData.initPTS > 0) {
                    const streamOffset = (Math.abs(eventData.initPTS) / DemoVOPlayer.DEFAULT_PTS_FREQUENCY) * 1000;
                    this.streamOffset = Math.round(streamOffset);
                }
            });
        }

        const fragCurrent = hls.streamController.fragCurrent;
        if (fragCurrent?.programDateTime) {
            this.usingProgramDateTime = true;

            this.rememberedPosition = this.player.currentTime;
            this.streamOffset = Math.round(fragCurrent?.programDateTime);
        }

        hls.on(Hls.Events.FRAG_CHANGED, (event: any, eventData: any) => {
            if (eventData.frag.programDateTime) {
                this.usingProgramDateTime = true;

                this.rememberedPosition = this.player.currentTime;
                this.streamOffset = Math.round(eventData.frag.programDateTime);
            }
        });
    }

    public unload() {
        // destroy() takes care of everything
    }

    public getPrecisionThreshold() {
        if (this.usingProgramDateTime) {
            return 50;
        }

        return 5;
    }

    public isSeekable(): boolean {
        return !this.player.isLive();
    }

    public isStalled(): boolean {
        return this.player.buffering_;
    }

    public play(): void {
        try {
            this.player.play();
        } catch (e) {
            console.error('Play error:', e);
        }
    }

    public pause(): void {
        try {
            this.player.pause();
        } catch (e) {
            console.error('Pause error:', e);
        }
    }

    public mute(): void {
        try {
            this.player.video_.muted = true;
        } catch (e) {
            console.log('Mute error:', e);
        }
    }

    public unmute(): void {
        try {
            this.player.video_.muted = false;
        } catch (e) {
            console.log('Unmute error:', e);
        }
    }

    public getCurrentPosition(): number {
        try {
            if (this.hls) {
                return Math.round(this.streamOffset + (this.player.currentTime - this.rememberedPosition) * 1000);
            }

            return Math.round(((this.player.programInfo().start || 0) + this.player.prftNtpOffset + this.player.currentTime) * 1000);
        } catch (e) {
            console.error('Get current position error:', e);
        }

        return 0;
    }

    public fastSeekToPosition(position: number) {
        if (position != null) {
            try {
                if (this.hls) {
                    this.player.video_.currentTime = (position - this.streamOffset + this.rememberedPosition * 1000) / 1000;
                } else {
                    this.player.currentTime = position / 1000 - this.player.prftNtpOffset - (this.player.programInfo().start || 0);
                }
            } catch (e) {
                console.error('Fast seek error:', e);
            }
        }
    }

    public isPlaying(): boolean {
        try {
            return this.player.playing;
        } catch (e) {
            console.error('Is playing error:', e);
        }

        return false;
    }

    public changePlaybackRate(rate: number): void {
        try {
            this.player.video_.playbackRate = rate;
        } catch (e) {
            console.error('Set playback rate error:', e);
        }
    }

    public getPlaybackRate(): number {
        try {
            return this.player.video_.playbackRate;
        } catch (e) {
            console.error('Get playback rate error:', e);
        }

        return 0;
    }

    public setVolume(volume: number) {
        try {
            this.player.video_.volume = volume;
        } catch (e) {
            console.error('Set volume error:', e);
        }
    }
}
