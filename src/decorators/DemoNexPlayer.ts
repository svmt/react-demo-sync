import { PlayerDecorator } from '@sscale/syncsdk';
import Hls from 'hls.js';
import { isSafariOriOS } from '../helper/device';

export class DemoNexPlayer extends PlayerDecorator {
    private static DEFAULT_PTS_FREQUENCY = 90000;

    private volumeBeforeMute = 0;

    private streamOffset = 0;
    private usingProgramDateTime = false;
    private rememberedPosition = 0;

    private isLive() {
        return this.player.isLive();
    }

    public load() {
        if (!Hls.isSupported() || !this.isLive()) {
            return;
        }

        const hls = this.player.nexplayer_.hls_;
        const initPTS = hls.streamController.initPTS;
        if (initPTS[0]) {
            const streamOffset = (Math.abs(initPTS[0]) / DemoNexPlayer.DEFAULT_PTS_FREQUENCY) * 1000;
            this.streamOffset = Math.round(streamOffset);
        } else {
            hls.on(Hls.Events.INIT_PTS_FOUND, (event: any, eventData: any) => {
                if (eventData.initPTS > 0) {
                    const streamOffset = (Math.abs(eventData.initPTS) / DemoNexPlayer.DEFAULT_PTS_FREQUENCY) * 1000;
                    this.streamOffset = Math.round(streamOffset);
                }
            });
        }

        const fragCurrent = hls.streamController.fragCurrent;
        if (fragCurrent?.programDateTime) {
            this.usingProgramDateTime = true;

            this.rememberedPosition = this.player.videoElem_.currentTime;
            this.streamOffset = Math.round(fragCurrent?.programDateTime);
        }

        hls.on(Hls.Events.FRAG_CHANGED, (event: any, eventData: any) => {
            if (eventData.frag.programDateTime) {
                this.usingProgramDateTime = true;

                this.rememberedPosition = this.player.videoElem_.currentTime;
                this.streamOffset = Math.round(eventData.frag.programDateTime);
            }
        });
    }

    public getPrecisionThreshold() {
        // Improve experience with stuttering issue on iOS and Safari
        if (isSafariOriOS()) {
            return 350;
        }

        if (this.usingProgramDateTime) {
            return 50;
        }

        return 10;
    }

    public isSeekable(): boolean {
        return !this.player.isLive();
    }

    public isStalled(): boolean {
        return this.player.videoElem_.readyState < this.player.videoElem_.HAVE_FUTURE_DATA;
    }

    public play(): void {
        try {
            if (!this.isPlaying()) {
                this.player.togglePlayPause();
            }
        } catch (e) {
            console.error('Play error:', e);
        }
    }

    public pause(): void {
        try {
            if (this.isPlaying()) {
                this.player.togglePlayPause();
            }
        } catch (e) {
            console.error('Pause error:', e);
        }
    }

    public mute(): void {
        try {
            this.volumeBeforeMute = this.player.getVolume();
            this.player.setVolume(0);
        } catch (e) {
            console.log('Mute error:', e);
        }
    }

    public unmute(): void {
        try {
            this.player.setVolume(this.volumeBeforeMute);
            this.volumeBeforeMute = 0;
        } catch (e) {
            console.log('Unmute error:', e);
        }
    }

    public getCurrentPosition(): number {
        try {
            return Math.round(
                this.streamOffset + (this.player.videoElem_.currentTime - this.rememberedPosition) * 1000,
            );
        } catch (e) {
            console.error('Get current position error:', e);
        }

        return 0;
    }

    public fastSeekToPosition(position: number) {
        if (this.isLive()) {
            return;
        }

        if (position != null) {
            try {
                const time = (position - this.streamOffset + this.rememberedPosition * 1000) / 1000;

                this.player.seek(time);
            } catch (e) {
                console.error('Fast seek error:', e);
            }
        }
    }

    public isPlaying(): boolean {
        try {
            return this.player.isPlaying();
        } catch (e) {
            console.error('Is playing error:', e);
        }

        return false;
    }

    public changePlaybackRate(rate: number): void {
        try {
            // Improve experience with stuttering issue on iOS and Safari
            if (isSafariOriOS()) {
                if (rate > 1) {
                    rate = Math.ceil(rate * 10) / 10 + 0.3;
                }

                if (Math.ceil(rate * 100) / 100 === Math.ceil(this.getPlaybackRate() * 100) / 100) {
                    return;
                }
            }

            this.player.setSpeed(rate);
        } catch (e) {
            console.error('Set playback rate error:', e);
        }
    }

    public getPlaybackRate(): number {
        try {
            return this.player.getPlaybackSpeed();
        } catch (e) {
            console.error('Get playback rate error:', e);
        }

        return 0;
    }

    public setVolume(volume: number) {
        try {
            this.player.setVolume(volume);
        } catch (e) {
            console.error('Set volume error:', e);
        }
    }
}
