import { PlayerDecorator } from '@sscale/syncsdk';
import { isSafariOriOS } from '../helper/device';

export class DemoVimeoPlayer extends PlayerDecorator {
    private currentPosition: number = 0;
    private currentPlaybackRate: number = 0;
    private paused: boolean = true;
    private isLive: boolean = false;
    private buffering: boolean = false;
    private timeout: number | undefined;
    private loaded: boolean = false;

    public load() {
        this.loaded = true;

        const onPlaying = () => {
            this.paused = false;

            this.player.getDuration().then((duration: number) => {
                this.isLive = Boolean(duration);
            });

            this.queryProgress();
        };

        this.player.getPaused().then((paused: boolean) => {
            if (!paused) {
                onPlaying();
            }
        });

        this.player.on('playing', onPlaying);

        this.player.on('pause', () => {
            console.log('pause');
            this.paused = true;

            clearTimeout(this.timeout);
        });

        this.player.on('bufferstart', () => {
            this.buffering = true;
        });

        this.player.on('bufferend', () => {
            this.buffering = false;
        });
    }

    public unload() {
        this.loaded = false;
        clearTimeout(this.timeout);
    }

    private queryProgress() {
        clearTimeout(this.timeout);

        if (this.paused || !this.loaded) {
            return;
        }

        // @ts-ignore
        this.timeout = setTimeout(async () => {
            const [position, playbackRate] = await Promise.all([
                this.player.getCurrentTime(),
                this.player.getPlaybackRate(),
            ]);
            this.currentPosition = position * 1000;
            this.currentPlaybackRate = playbackRate;

            this.queryProgress();
        }, 25);
    }

    public getPrecisionThreshold() {
        // Improve experience with stuttering issue on iOS and Safari
        if (isSafariOriOS()) {
            return 350;
        }

        return 5;
    }

    public isStalled(): boolean {
        return this.buffering;
    }

    public isSeekable(): boolean {
        return !this.isLive;
    }

    public play(): void {
        this.player.play().catch((e: any) => {
            console.error('Play error:', e);
        });
    }

    public mute(): void {
        this.player.setMuted(true).catch((e: any) => {
            console.error('Mute error:', e);
        });
    }

    public unmute(): void {
        this.player.setMuted(false).catch((e: any) => {
            console.error('Unmute error:', e);
        });
    }

    public pause(): void {
        this.player.pause().catch((e: any) => {
            console.error('Pause error:', e);
        });
    }

    public getCurrentPosition(): number {
        return this.currentPosition;
    }

    public fastSeekToPosition(position: number) {
        if (position != null) {
            this.player.setCurrentTime(position / 1000).catch((e: any) => {
                console.error('Fast seek error:', e);
            });
        }
    }

    public isPlaying(): boolean {
        return !this.paused;
    }

    public getPlaybackRate(): number {
        return this.currentPlaybackRate;
    }

    public changePlaybackRate(rate: number): void {
        // Improve experience with stuttering issue on iOS and Safari
        if (isSafariOriOS()) {
            if (rate > 1) {
                rate = Math.ceil(rate * 10) / 10 + 0.3;
            }

            if (Math.ceil(rate * 100) / 100 === Math.ceil(this.getPlaybackRate() * 100) / 100) {
                return;
            }
        }

        this.player.setPlaybackRate(rate).catch((e: any) => {
            console.error('Set playback rate error:', e);
        });
    }

    public setVolume(volume: number) {
        this.player.setVolume(volume).catch((e: any) => {
            console.error('Set volume error:', e);
        });
    }
}
