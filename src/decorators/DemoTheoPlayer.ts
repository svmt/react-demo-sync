import { PlayerDecorator } from '@sscale/syncsdk';

export class DemoTheoPlayer extends PlayerDecorator {
    private isLive(): boolean {
        return this.player.duration === Infinity;
    }

    public isSeekable(): boolean {
        try {
            return !this.isLive();
        } catch (e) {
            console.error('Is seekable error', e);
        }

        return false;
    }

    public isStalled(): boolean {
        // 3 - HAVE_FUTURE_DATA
        return this.player.readyState < 3;
    }

    public play(): void {
        try {
            this.player.play();
        } catch (e) {
            console.error('Play error:', e);
        }
    }

    public mute(): void {
        try {
            this.player.muted = true;
        } catch (e) {
            console.error('Mute error:', e);
        }
    }

    public unmute(): void {
        try {
            this.player.muted = false;
        } catch (e) {
            console.error('Unmute error:', e);
        }
    }

    public pause(): void {
        try {
            this.player.pause();
        } catch (e) {
            console.error('Pause error:', e);
        }
    }

    public getCurrentPosition(): number {
        try {
            if (this.isLive()) {
                return this.player.currentProgramDateTime.getTime();
            }

            return Math.round(this.player.currentTime * 1000);
        } catch (e) {
            console.error('Get current position error:', e);
        }

        return 0;
    }

    public fastSeekToPosition(position: number) {
        if (position != null) {
            try {
                this.player.currentTime = position / 1000;
            } catch (e) {
                console.error('Fast seek error:', e);
            }
        }
    }

    public isPlaying(): boolean {
        try {
            return !this.player.paused;
        } catch (e) {
            console.error('Is playing error:', e);
        }

        return false;
    }

    public getPlaybackRate(): number {
        try {
            return this.player.playbackRate;
        } catch (e) {
            console.error('Get playback rate error:', e);
        }

        return 0;
    }

    public changePlaybackRate(rate: number): void {
        try {
            this.player.playbackRate = rate;
        } catch (e) {
            console.error('Set playback rate error:', e);
        }
    }

    public setVolume(volume: number) {
        try {
            this.player.volume = volume;
        } catch (e) {
            console.error('Set volume error:', e);
        }
    }
}
