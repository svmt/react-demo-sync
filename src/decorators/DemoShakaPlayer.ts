import { PlayerDecorator } from '@sscale/syncsdk';
import { isSafariOriOS } from '../helper/device';

// this.player is shaka instance
export class DemoShakaPlayer extends PlayerDecorator {
    public isHLS() {
        return /m3u8/i.test(this.player.getAssetUri());
    }

    public isLive() {
        return this.player.isLive();
    }

    public getPrecisionThreshold() {
        // Improve experience with stuttering issue on iOS and Safari
        if (isSafariOriOS()) {
            return 350;
        }

        return 50;
    }

    public isStalled() {
        try {
            return this.player.isBuffering();
        } catch (e) {
            console.error('Is stalled error', e);
        }

        return false;
    }

    public isSeekable() {
        const range = this.player.seekRange();

        if (!range) {
            return false;
        }

        // Only if seek range is more than 20 seconds
        return range.end - range.start > 20;
    }

    public play() {
        try {
            this.player.getMediaElement().play();
        } catch (e) {
            console.error('Play error:', e);
        }
    }

    public mute() {
        try {
            this.player.getMediaElement().muted = true;
        } catch (e) {
            console.log('Mute error:', e);
        }
    }

    public unmute() {
        try {
            this.player.getMediaElement().muted = false;
        } catch (e) {
            console.log('Unmute error:', e);
        }
    }

    public pause() {
        try {
            this.player.getMediaElement().pause();
        } catch (e) {
            console.error('Pause error:', e);
        }
    }

    public getLiveEdgePosition() {
        //The general idea should be smth like this:
        //
        // live edge position = liveEdge
        // When live edge info was created = liveEdgeDate
        // TARGETDURATION
        // Actual maximum position in each moment = (liveEdge - TARGETDURATION) + (Date.now() - liveEdgeDate)
        // #EXT-X-TARGETDURATION:7 hls, need to subtract 7 seconds from end
        // using hls.js the easiest and best candidate would be hls.liveSyncPosition
        // with hls you are not able to seek to end seek position, but with dash you can
        return super.getLiveEdgePosition();
    }

    public getCurrentPosition() {
        try {
            // Use getPlayheadTimeAsDate, current time is not absolute when HLS is used with live
            if (this.isHLS() && this.isLive()) {
                console.log('islive');
                return this.player.getPlayheadTimeAsDate().getTime();
            }

            return Math.round(this.player.getMediaElement().currentTime * 1000);
        } catch (e) {
            console.error('Get current position error:', e);
        }

        return 0;
    }

    public fastSeekToPosition(position: number) {
        if (position == null) {
            return;
        }

        try {
            // Use getPlayheadTimeAsDate, current time is not absolute when HLS is used with live
            if (this.isHLS() && this.isLive()) {
                const currentDatePosition = this.player.getPlayheadTimeAsDate().getTime();

                this.player.getMediaElement().currentTime =
                    position - currentDatePosition + this.player.getMediaElement().currentTime;
            } else {
                this.player.getMediaElement().currentTime = position / 1000;
            }
        } catch (e) {
            console.error('Fast seek error:', e);
        }
    }

    public isPlaying() {
        try {
            return !this.player.getMediaElement().paused;
        } catch (e) {
            console.error('Is playing error:', e);
        }

        return false;
    }

    public changePlaybackRate(rate: number) {
        try {
            // Improve experience with stuttering issue on iOS and Safari
            if (isSafariOriOS()) {
                if (rate > 1) {
                    rate = Math.ceil(rate * 10) / 10 + 0.3;
                }

                if (Math.ceil(rate * 100) / 100 === Math.ceil(this.getPlaybackRate() * 100) / 100) {
                    return;
                }
            }

            this.player.getMediaElement().playbackRate = rate;
        } catch (e) {
            console.error('Set playback rate error:', e);
        }
    }

    public getPlaybackRate() {
        try {
            return this.player.getMediaElement().playbackRate;
        } catch (e) {
            console.error('Get playback rate error:', e);
        }

        return 0;
    }

    public setVolume(volume: number) {
        try {
            this.player.getMediaElement().volume = volume;
        } catch (e) {
            console.error('Set volume error:', e);
        }
    }
}
